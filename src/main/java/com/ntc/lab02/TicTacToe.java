package com.ntc.lab02;

import java.util.Scanner;

/**
 *
 * @author L4ZY
 */
public class TicTacToe {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    static int count = 0;
    Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showWelcome() {
        System.out.println("Welcome to TicTacToe Game");
    }

    public static void showTable() {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Pleaes input row, col");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                System.out.println(">>> " + currentPlayer + " win <<<");
                return;
            }
            count++;
            if (checkDraw()) {
                showDraw();
            }

            switchPlayer();
        }
    }

    static boolean checkDraw() {
        if (count == 9) {
            return finish = true;

        }
        return false;

    }

    public static void showDraw() {
        showTable();
        System.out.println(">>> draw!!! <<<");

    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static void showWin() {
        System.out.println(">>> " + currentPlayer + " Win <<<");
    }

    public static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;

    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

}
